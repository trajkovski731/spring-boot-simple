package com.trajkovski.demoH2.service;

import com.trajkovski.demoH2.entity.User;
import com.trajkovski.demoH2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User addUser(final User user) {
        return repository.save(user);
    }

    public List<User> findAllUsers() {
        return repository.findAll();
    }

    public User updateUser(final User user) {
        return repository.save(user);
    }

    public void removeUser(final Long id) {
        repository.deleteById(id);
    }

    public User findUserById(final Long id) {
        return repository.getOne(id);
    }


}
