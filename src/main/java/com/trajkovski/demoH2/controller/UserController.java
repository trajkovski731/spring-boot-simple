package com.trajkovski.demoH2.controller;

import com.trajkovski.demoH2.entity.User;
import com.trajkovski.demoH2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public User addUser(@RequestBody final User user) {
        return userService.addUser(user);
    }

    @PutMapping(value = "/{id}")
    public User updateUser(@RequestBody final User user, @PathVariable final Long id) {
        return userService.updateUser(user);
    }

    @GetMapping
    public List<User> findAllUsers() {
        return userService.findAllUsers();
    }

    @GetMapping(value = "/{id}")
    public User findUserById(@PathVariable final Long id) {
        return userService.findUserById(id);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable final Long id) {
        userService.removeUser(id);
    }

}
